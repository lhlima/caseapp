#!/bin/sh

ENV="dev"

if [ "$1" = "prod" ]; then
  ENV="prod"
fi

echo "---------------------------------------------------------------"
echo "Formatting terraform files"
terraform fmt
echo "---------------------------------------------------------------"
terraform init -backend=true -backend-config="${ENV}/backend.hcl"
echo "---------------------------------------------------------------"
echo "Validating terraform files"
terraform validade
echo "---------------------------------------------------------------"
echo "Planning..."
terraform plan -var-file="${ENV}/terraform.tfvars" -out="plan.tfout"
echo "---------------------------------------------------------------"
echo "Updating environment..."
terraform apply plan.tfout
echo "---------------------------------------------------------------"
echo "Cleaning up plan files!"
rm -rf plan.tfout