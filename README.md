# Case para ThoughtWorks

## Objetivo:
    Desenvolver uma aplicação aplicando a prática DevOps contendo Integração e Distribuição Contínua.
    Aplicação de versionamento semântico.
    Criação de Infraestrutura como Código automatizando o processo e gerenciamento dos ambientes.
    Aplicar processo de monitoramento da aplicação.


## Solução:

### NodeJs / Docker Container
    Para demonstrar as práticas solicitas no objetivo, foi desenvolvida uma aplicação de teste utilizando *NodeJs* juntamente com *Docker Container* 
    Como forma de gerenciamento do código e controle de versão, foi utilizado como prática o *GitFlow* em um repositório GitLab, de forma que as branchs deverão ser criadas conforme andamento das sprints e distribuição de equipes.
- gitflow.png ![](png.png)

### Terraform
    A estrutura atual utilizada do terraform é composta de dois tipos distintos de configuração, uma apenas de módulos reutilizáveis onde qualquer estrutura de pastas e/ou organização pode ser usada e outro conjunto de pastas que realiza as chamadas ao módulos e tem sua propria estrutura de pastas, além de iniciar com o nome do backend a qual aquela estrutura pertence.

### Modulos
    A pasta módulos na raiz do projeto se destina aos modulos comumente utilizados no terraform, e segue o seguinte padrão:
    .
    └── modulo
        ├── main.tf
        ├── outputs.tf
        └── vars.tf

A estrutura de nomes segue o padrão comumente utilizado por projetos terraform, onde o `main.tf` contém todas as chamadas aos resources, o arquivo `vars.tf` as variáveis possíveis do módulo e seus padrões e o arquivo `outputs.tf` recebe os valores a serem exibidos ao término da execução do módulo, assim como o armazenamento desses valores no arquivo state.


### Monitoramento - New Relic
    A aplicação contem configuração para utilização de sistem de monitoramento com *New Relic*
    Como estamos tratando de uma avaliação, as credenciais contendo a configuração para um ambiente produtivo foram inibidas.

---------
# Ciclo de Vida da Aplicação

    Para demonstrar e exemplificar funcionamento do fluxo de *CI* e *CD* deve-se realizar um *Pull Request na Master ou Commit em alguma branch*.
    Após realizado esse processo, será iniciado a build a aplicação validando as alterações realizadas. O processo de build irá utilizar o arquivo *.gitlab-ci.yml* como estrutura para build da infraestrutura e aplicação.
    
    A infraestrutura como código foi desenvolvida para ser utilizada na Cloud AWS, contendo as seguintes definições:
    .
    └── environment (ex: dev/stg/prd)
        ├── Application
        │   ├──  Application Load Balancer
        │   └──  Auto Scaling
        ├── Security
        │   ├── VPC
        │   ├── Network
        │   └── Internet Gateway
        ├── Services
        │   └── ECS
        ├── ECR
        ├── IAM
        ├── Monitoramento
           └──  Cloud Watch    
    
    
    