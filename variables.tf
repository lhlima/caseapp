variable "region" {
  default = "us-east-2"
}

variable "env" {
  default = "dev"
}

variable "app_name" {
  default = "appNodeJs"
}

variable "app_folder" {
  default = "./appNodeJs"
}

variable "az_count" {
  default = 2
}

variable "ecs_task_role_name" {
  default = "EcsTaskExecutionRole"
}

variable "fargate_cpu" {
  default = 512
}

variable "fargate_memory" {
  default = 1024
}

variable "app_port" {
  default = 3000
}

variable "app_count" {
  default = 2
}

variable "health_check_path" {
  default = "/"
}

variable "ecs_auto_scale_role_name" {
  default = "EcsAutoScaleRole"
}

variable "ac_min_capacity" {
  default = 2
}

variable "ac_max_capacity" {
  default = 3
}

variable "cidrblock" {
  default = "10.1.0.0/16"
}

variable "log_retential" {
  default = 5
}

variable "access_key" {
  default = ""
}

variable "secret_key" {
  default = ""
}